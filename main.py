from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time
from bs4 import BeautifulSoup
import re
import random
import pandas as pd
import ast

# Загружаем существующий файл Excel, если он уже существует
# Load an existing Excel file if it already exists
df = pd.DataFrame()

try:
    existing_data = pd.read_excel('questions_answers.xlsx')
    df = pd.concat([existing_data, df], ignore_index=True)
except FileNotFoundError:
    # Создаем DataFrame с вопросом и ответами
    # Create a DataFrame with the question and answers
    data = {'Question': [], 'Answers': []}
    df = pd.DataFrame(data)
    df.to_excel('questions_answers.xlsx', index=False)

def replace_latin_to_cyrillic(string):
    # Создание словаря для замены символов
    # Create a dictionary for character replacement
    mapping = {
        'a': 'а',
        'b': 'б',
        'c': 'с',
        'e': 'е',
        'o': 'о',
        'y': 'у',
        'p': 'р',
        'x': 'х',
    }

    result = ""
    for char in string:
        if char.lower() in mapping:
            result += mapping[char.lower()]
        else:
            result += char

    return result


def save_to_excel(question, answers, df):
    # Создаем новый DataFrame с новыми данными
    # Create a new DataFrame with the new data
    new_data = {'Question': [question], 'Answers': [answers]}
    new_df = pd.DataFrame(new_data)

    # Объединяем новый DataFrame с существующим DataFrame
    # Concatenate the new DataFrame with the existing DataFrame
    df = pd.concat([df, new_df], ignore_index=True)

    # Возвращаем обновленный DataFrame
    # Return the updated DataFrame
    return df


def parse_question_and_answers(html_code):
    # Создание объекта BeautifulSoup
    # Create a BeautifulSoup object
    soup = BeautifulSoup(html_code, 'html.parser')

    # Поиск элемента с вопросом
    # Find the element with the question
    pattern = r'<td align="center">(.*?)<br></td>'

    # Получение текста вопроса
    # Get the question text
    question = replace_latin_to_cyrillic(re.search(pattern, html_code).group(1))

    # Поиск элементов с вариантами ответа
    # Find the elements with the answer options
    answer_elements = soup.find_all('label')

    # Получение текстов вариантов ответа
    # Get the texts of the answer options
    answers = [replace_latin_to_cyrillic(answer.text) for answer in answer_elements]

    original_answers = [answer.text for answer in answer_elements]

    return question, answers, original_answers


def check_element_exists(html_code):
    soup = BeautifulSoup(html_code, 'html.parser')
    element = soup.find('h4', string='на предыдущий вопрос был дан неверный/неполный ответ')
    return element is not None


def f(df):
    # options
    options = webdriver.ChromeOptions()

    # user-agent
    options.add_argument("user-agent=Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0")

    driver = webdriver.Chrome(
        options=options
    )
    try:
        driver.get("https://tester.pp.ru/")

        tn_input = driver.find_element("id", "tn")
        tn_input.clear()
        tn_input.send_keys("STUDENT_KEY")

        element = driver.find_element(By.XPATH, '//a[@onclick="loginme(); return true;"]')
        element.click()

        # Получение списка доступных окон
        # Getting a list of available windows
        window_handles = driver.window_handles

        # Переключение на последнее окно (новую страницу)
        # Switch to the last window (new page)
        driver.switch_to.window(window_handles[-1])

        # Найти элемент по его name-атрибуту
        # Find an element by its name attribute
        element = driver.find_element("name", "exam")

        # Очистить поле ввода
        # Clear input field
        element.clear()
        element.send_keys("CLASS_NUM")

        element = driver.find_element(By.CSS_SELECTOR, 'input[type="submit"][value="ok"][class="button"]')
        element.click()

        element = driver.find_element(By.CSS_SELECTOR, 'input[type="submit"][value="НАЧАТЬ ТЕСТ"][class="button"]')
        element.click()

        for _ in range(50):

            # Получение кода страницы
            # Getting the page code
            page_source = driver.page_source


            question, answers, original_answers = parse_question_and_answers(page_source)

            soup = BeautifulSoup(page_source, "html.parser")

            # Поиск первого элемента input с атрибутами type="radio"
            # Finding the first input element with type="radio" attributes
            radio_button = soup.find("input", attrs={"type": "radio"})

            # Получение текстового значения элемента
            # Getting the text value of an element
            text_value = radio_button.next_sibling.strip()
            input_name = radio_button.get('name')

            web_element = driver.find_element(By.CSS_SELECTOR, f'input[type="radio"][name="{input_name}"]')

            web_element.click()

            element = driver.find_element(By.CSS_SELECTOR,
                                          'input[type="submit"][name="go"][value="Ответить"][class="button"]')
            element.click()
            df['Вопрос'] = df['Вопрос'].astype(str)
            page_source = driver.page_source
            if not check_element_exists(page_source) and not df['Вопрос'].str.lower().isin([question]).any():
                print('Вопрос:', question)
                print('Ответ:', text_value)
                df = save_to_excel(question, text_value, df)
                df.to_excel('questions_answers.xlsx', index=False)

        return df
    except Exception as ex:
        print(ex)
    finally:
        driver.close()
        driver.quit()
    return df


for i in range(200):
    print('Прохождение теста номер', i)

    df = f(df)