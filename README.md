## Automatic Test Taking and Answer Recording Program

This program is designed to automatically take a test and record the correct answers in an Excel file. It is useful in situations where a student does not have direct access to the test questions and needs to manually take the test multiple times.

### Usage

1. Install the necessary dependencies, such as Selenium, BeautifulSoup, and pandas.
2. Run the program, ensuring that the test parameters are properly configured.
3. The program will automatically launch the Chrome web browser, open the tester.pp.ru website, and proceed with the test.
4. The correct answers will be recorded in an Excel file.

### Configuration of Test Parameters

Before running the program, ensure to configure the following parameters:

- STUDENT_KEY: Replace with your student key for authentication on the tester.pp.ru website.
- CLASS_NUM: Replace with the number of your class or group.
- ```for _ in range(N)``` (line 139): Adjust the number of questions in the test as required (within the available number of questions on the website).
- ```for i in range(Z)``` (line 183): Adjust the number of test iterations as required (within the available number of questions on the website).

### Important Information

Please note that the use of this program to automatically take a test may be in violation of your university or instructor's rules and policies. Before using the program, ensure that you have the necessary permissions and do not violate any rules. The author is not responsible for any misuse of the program.

Good luck with your test!

---

## Программа для прохождения и сохранения ответов на автоматический тест

Данная программа предназначена для автоматического прохождения теста и записи правильных ответов в файл Excel. Она полезна для случаев, когда у студента нет непосредственного доступа к вопросам теста, и он должен пройти его "руками" множество раз.

### Использование программы

1. Установите необходимые зависимости, такие как Selenium, BeautifulSoup и pandas.
2. Запустите программу, предварительно сконфигурировав параметры теста.
3. Программа будет автоматически запускать веб-браузер Chrome, открывать сайт tester.pp.ru и проходить тест.
4. Верные ответы будут записываться в файл Excel.

### Конфигурация параметров теста

Перед запуском программы следует настроить следующие параметры:

- STUDENT_KEY - замените на ваш ключ студента для авторизации на сайте tester.pp.ru.
- CLASS_NUM - замените на номер вашего класса или группы.
- ```for _ in range(N)```(139 строка) - замените на необходимое количество вопросов в тесте (можно изменять в пределах доступного количества вопросов на сайте).
- ```for i in range(Z)```(183 строка) - замените на необходимое количество прохождений теста (можно изменять в пределах доступного количества вопросов на сайте).

### Важная информация

Пожалуйста, обратите внимание, что использование программы для автоматического прохождения теста может противоречить правилам и политике вашего университета или преподавателя. Перед использованием программы убедитесь, что у вас есть право на такие действия и не нарушаете никаких правил. Автор не несет ответственности за неправомерное использование программы.


Удачи в прохождении вашего теста!